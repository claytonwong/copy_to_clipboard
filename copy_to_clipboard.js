var toggleFeaturePressCount = 0; // Count of number of times "Ctrl+Shift+U" is pressed
var otherFeaturePressCount = 0; // Count of number of times "Ctrl+Shift+B" is pressed
var text = []; // Text to copy to clipboard
var selectedText = []; // Text selected with mouse

browser.commands.onCommand.addListener((command) => {
  browser.tabs.query({currentWindow: true, active: true})
  .then((tabs) => {
        
    if(command === "toggle-feature")
    {
      console.log('Toggle feature used')

      toggleFeaturePressCount ++
      console.log('toggleFeaturePressCount: ', toggleFeaturePressCount)
      
      if (toggleFeaturePressCount === 1) {
        text = "Title: " + tabs[0].title;
      }
      else if (toggleFeaturePressCount === 2) {
        text = "Source: "
      }
      else {
        text = "Url: " + tabs[0].url;
      }
      
      if(toggleFeaturePressCount % 3 === 0) {
        toggleFeaturePressCount = 0
      }
    }
    else if (command === "other-feature")
    {
      console.log('Other feature used')

      otherFeaturePressCount ++
      console.log('otherFeaturePressCount: ', otherFeaturePressCount)
      
      if (otherFeaturePressCount === 1) {
        text = "Number: ";
      }
      else if (otherFeaturePressCount === 2) {
        text = "Source & num: ";
      }
      else {
        text = "Url: " + tabs[0].url;
      }
      
      if(otherFeaturePressCount % 3 === 0) {
        otherFeaturePressCount = 0
      }
    }
    
    // The example will show how data can be copied, but since background
    // pages cannot directly write to the clipboard, we will run a content
    // script that copies the actual content.

    // clipboard-helper.js defines function copyToClipboard.
    const code = "copyToClipboard(" +
      JSON.stringify(text) + "," +
      ");";

    browser.tabs.executeScript({
      code: "typeof copyToClipboard === 'function';",
    }).then((results) => {
      // The content script's last expression will be true if the function
      // has been defined. If this is not the case, then we need to run
      // clipboard-helper.js to define function copyToClipboard.
      if (!results || results[0] !== true) {
        return browser.tabs.executeScript(tabs[0].id, {
          file: "clipboard-helper.js",
        });
      }
    }).then(() => {
      return browser.tabs.executeScript(tabs[0].id, {
        code,
      });
    }).catch((error) => {
      // This could happen if the extension is not allowed to run code in
      // the page, for example if the tab is a privileged page.
      console.error("Failed to copy text: " + error);
    });
  })
  .catch((error) => {
    console.log(`Error: ${error}`);
  })
});
